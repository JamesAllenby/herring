TARGET = herring

CC     = i686-elf-gcc
QEMU   = qemu-system-i386
CFLAGS = -ffreestanding -nostdlib -lgcc -O3

SRCS   = boot.s kernel.c terminal.c

all: $(TARGET)

$(TARGET):
	$(CC) $(CFLAGS) -T linker.ld $(SRCS) -o $(TARGET)

run: $(TARGET)
	$(QEMU) -kernel $(TARGET)

clean:
	rm $(TARGET)