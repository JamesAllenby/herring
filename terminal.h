#ifndef TERMINAL_H
#define TERMINAL_H

#include <stddef.h>
#include <stdint.h>

// Terminal dimensions
static const size_t TERM_WIDTH = 80;
static const size_t TERM_HEIGHT = 25;

// terminal_initialise initialises the terminal buffer
void terminal_initialise(void);

// terminal_reset sets the terminal back to default values and clears the screen
void terminal_reset(void);

// terminal_clear retains its colour settings and clears the screen
void terminal_clear(void);

// terminal_putcharat places a single character at a co-ordinate
void terminal_putcharat(char c, size_t x, size_t y);

// terminal_putchar places a single character at the start location or alongside the last character
void terminal_putchar(char c);

// terminal_write prints out an array of characters defined by length
void terminal_write(const char* str, size_t len);

// terminal_writestring prints out an array of characters
void terminal_writestring(const char* str);

// terminal_setbg sets the background colour for characters printed after this call
void terminal_setbg(uint8_t bg);

// terminal_setfg sets the foreground colour for characters printed after this call
void terminal_setfg(uint8_t fg);

#endif