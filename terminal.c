#include "terminal.h"

#include <stddef.h>
#include <stdint.h>

#include "termcolor.h"
#include "string.h"

volatile uint16_t* terminal_buffer;
uint8_t terminal_colour;
size_t terminal_row;
size_t terminal_col;

void terminal_setbg(uint8_t bg)
{
  if (bg > 15)
    return;
  terminal_colour = terminal_colour ^ (bg << 4);
}

void terminal_setfg(uint8_t fg)
{
  if (fg > 15)
    return;
  terminal_colour = terminal_colour ^ (fg << 0);
}

void terminal_writestring(const char* str)
{
  terminal_write(str, strlen(str));
}

void terminal_write(const char* str, size_t len)
{
  for (size_t idx = 0; idx < len; idx++)
    terminal_putchar(str[idx]);
}

void terminal_putchar(char c)
{
  if (c == 0)
    return;
  terminal_putcharat(c, terminal_col, terminal_row);
  if (++terminal_col >= TERM_WIDTH) {
    terminal_col = 0;
    if (++terminal_row >= TERM_HEIGHT)
      terminal_row = 0;
  }
}

void terminal_putcharat(char c, size_t x, size_t y)
{
  const size_t idx = (y * TERM_WIDTH) + x;
  terminal_buffer[idx] = (uint16_t)c | (uint16_t)terminal_colour << 8;
}

void terminal_clear(void)
{
  for (size_t x = 0; x < TERM_WIDTH; x++)
    for (size_t y = 0; y < TERM_HEIGHT; y++)
      terminal_putcharat(' ', x, y);
  terminal_row = 0;
  terminal_col = 0;
}

void terminal_reset(void)
{
  terminal_setbg(TERMCOLOR_BLACK);
  terminal_setfg(TERMCOLOR_WHITE);
  terminal_clear();
}

void terminal_initialise(void)
{
  terminal_buffer = (uint16_t*)0xB8000;
  terminal_reset();
}
