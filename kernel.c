#include <stddef.h>
#include <stdint.h>

#include "terminal.h"

void kernel_main()
{
  terminal_initialise();

  // Print kernel name on-screen
  terminal_writestring("Herring");
}
