# Export _start function
.global _start

# Multiboot constants
.set ALIGN,    1<<0             # Aligned on 4KiB boundries
.set MEMINFO,  1<<1             # Available memory flag
.set VIDMODE,  1<<2             # Video mode table flag
.set FLAGS,    ALIGN | MEMINFO  # Multiboot image flags
.set MAGIC,    0x1BADB002       # Multiboot magic value
.set CHECKSUM, -(MAGIC + FLAGS) # Checksum from magic value and flags

# Multiboot declaration
.section multiboot
.align 4
.int MAGIC
.int FLAGS
.int CHECKSUM

# Stack declaration
.bss
.align 16
stack_bottom:
.skip 16384 # 16 KiB
stack_top:

.text
_start:
  mov $stack_top,    %esp # Reset stack pointer to the top
  cli                     # Disable maskable interrupts
  call kernel_main        # Call kernel.c:kernel_main()
halt:
  hlt                     # Halt forever, interrupts disabled
  jmp halt                # NMI and software interrupts still
                          # exist, go back to halt if encountered
